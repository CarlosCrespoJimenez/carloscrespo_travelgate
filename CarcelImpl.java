/*@author Carlos Crespo*/

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;

//Clase CarcelImpl con sus métodos get, set y constructor.
public abstract class CarcelImpl implements Carcel{
    List<members> listaPersonasDentroCarcel;
    private final List<members> subordinados;
    
    public CarcelImpl(List<members> subordinados) {
        this.listaPersonasDentroCarcel = new ArrayList<>();
        this.subordinados = subordinados;  
    }
    
    //Método salir de la Cárcel
    public List<members> salirCarcel(List<members> listaPersonasDentroCarcel){
        boolean remove = listaPersonasDentroCarcel.remove(subordinados);
        return subordinados;
    }
    
    //Método entrar en la Cárcel
    public void entrarCarcel(List<members> listaPersonasDentroCarcel){
        listaPersonasDentroCarcel.add(0, (members) subordinados);
    }

    public List<members> getPersonas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterator<members> iterator() {
        return Carcel.super.iterator(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void forEach(Consumer<? super members> action) {
        Carcel.super.forEach(action); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Spliterator<members> spliterator() {
        return Carcel.super.spliterator(); //To change body of generated methods, choose Tools | Templates.
    }
}