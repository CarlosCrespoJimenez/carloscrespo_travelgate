import java.io.FileReader;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import java.util.*;

public class Principal {
        public static void main(String[] args) {
            /*Inicializamos y probamos
            String nombreMiembro = "Carlos";
            Integer antiguedad = 8;
            System.out.print("Nombre:" + nombreMiembro + "\n");
            System.out.print("Antiguedad:" + antiguedad + "\n");*/
        
        }   
        public class lee_json {
            public void main(String args[]) throws java.io.IOException {
                JsonParser parser = new JsonParser();
                FileReader fr = new FileReader("src/datos.json");
                JsonElement datos = parser.parse(fr);
                dumpJSONElement(datos);
            }
            


    public void dumpJSONElement(JsonElement elemento) {
    if (elemento.isJsonObject()) {
        System.out.println("nombreMiembro:");
        JsonObject obj = elemento.getAsJsonObject();
        java.util.Set<java.util.Map.Entry<String,JsonElement>> entradas = obj.entrySet();
        java.util.Iterator<java.util.Map.Entry<String,JsonElement>> iter = entradas.iterator();
        while (iter.hasNext()) {
            java.util.Map.Entry<String,JsonElement> entrada = iter.next();
            System.out.println("antiguedad: " + entrada.getKey());
            System.out.println("superior:");
            dumpJSONElement(entrada.getValue());
        }
 
    } else if (elemento.isJsonArray()) {
        JsonArray array = elemento.getAsJsonArray();
        System.out.println("Tamano Array: " + array.size());
        java.util.Iterator<JsonElement> iter = array.iterator();
        while (iter.hasNext()) {
            JsonElement entrada = iter.next();
            dumpJSONElement(entrada);
        }
    } else if (elemento.isJsonPrimitive()) {
        System.out.println("");
        JsonPrimitive valor = elemento.getAsJsonPrimitive();
        if (valor.isBoolean()) {
            System.out.println("" + valor.getAsBoolean());
        } else if (valor.isNumber()) {
            System.out.println("" + valor.getAsNumber());
        } else if (valor.isString()) {
            System.out.println("" + valor.getAsString());
        }
    } else if (elemento.isJsonNull()) {
        System.out.println("");
    } else {
        System.out.println("");
 }
 }
}}
