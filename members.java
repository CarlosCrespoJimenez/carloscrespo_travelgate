/** @author Carlos Crespo */

import java.util.List;

public interface members extends Iterable<members> {
    List<members> subordinados();
    String superior();
    String nombreMiembro();
    Integer antiguedad();  
}
