/* @author Carlos Crespo */

import java.util.*;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.stream.Stream;

//Clase membersImpl y sus métodos get, set y constructor
public abstract class membersImpl implements members {
    List<members> subordinados;
    String superior;
    String nombreMiembro;
    Integer antiguedad;

    //Constructor
    public membersImpl(List<members> subordinados) {
        this.subordinados = subordinados;
    }
  
    //Métodos get y set
    public List<members> getSubordinados() {
        return subordinados;
    }

    public String getSuperior() {
        return superior;
    }

    @Override
    public void forEach(Consumer<? super members> action) {
        members.super.forEach(action); //To change body of generated methods, choose Tools | Templates.
    }

    public Spliterator<members> spliterator() {
        return members.super.spliterator(); //To change body of generated methods, choose Tools | Templates.
    }

    public String getNombreMiembro() {
        return nombreMiembro;
    }

    public Integer getAntiguedad() {
        return antiguedad;
    }

    public void setSubordinados(List<members> subordinados) {
        this.subordinados = subordinados;
    }

    public void setSuperior(String superior) {
        this.superior = superior;
    }

    public void setNombreMiembro(String nombreMiembro) {
        this.nombreMiembro = nombreMiembro;
    }

    public void setAntiguedad(Integer antiguedad) {
        this.antiguedad = antiguedad;
    }
        

    public List<members> getPersonas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    public String superior() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String nombreMiembro() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Integer antiguedad() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Iterator<members> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
